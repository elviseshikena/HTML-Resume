// JavaScript source code
var init = 0;
var stop = false;
function DateTime() {
    var d = new Date();
    var year = d.getFullYear();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var hour = d.getHours();
    var minute = d.getMinutes();
    var second = d.getSeconds();
    var dofweek = d.getDay();

    var time = document.getElementById("time");
    var date = document.getElementById("date");

    switch (dofweek) {
        case 0:dofweek="SUN";break;
        case 1:dofweek="MON";break;
        case 2:dofweek="TUE";break;
        case 3:dofweek="WED";break;
        case 4:dofweek="THU";break;
        case 5:dofweek="FRI";break;
        case 6:dofweek="SAT";break;
    }
    switch (month) {
        case 1:month = "JAN";break;
        case 2:month = "FEB";break;
        case 3:month = "MAR";break;
        case 4:month = "APR";break;
        case 5:month = "MAY";break;
        case 6:month = "JUN";break;
        case 7:month = "JUL";break;
        case 8:month = "AUG";break;
        case 9:month = "SEP";break;
        case 10:month = "OCT";break;
        case 11:month = "NOV";break;
        case 12:month = "DEC";break;
    }
    time.innerHTML = n(hour) + ":" + n(minute);
    date.innerHTML = dofweek + " " + n(day) + "-" + month + "-" + n(year);

    if (stop != true){
        if (init == 2) {
            onStart(hour);
            stop = true;
        }
        init+=1;
    }
    
}

function rand(min,max) {
    return Math.floor(Math.random()*(max-min+1)+min);
}

function n(n){
    return n > 9 ? "" + n: "0" + n;
}

function hoot() {
    var x = document.getElementById("hoot");
    x.className = "show";
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
}

function GreetUser(hour) {
    var num = rand(0, 1);
    var t = document.getElementById("hoot");
    //system message appears half a scnd after each other.
    if (hour >= 0 && hour < 12) {
        t.innerHTML = 'Good morning  <svg style="width:24px;height:24px" viewBox="0 0 24 24">     <path fill="#fff" d="M20,12A8,8 0 0,0 12,4A8,8 0 0,0 4,12A8,8 0 0,0 12,20A8,8 0 0,0 20,12M22,12A10,10 0 0,1 12,22A10,10 0 0,1 2,12A10,10 0 0,1 12,2A10,10 0 0,1 22,12M10,9.5C10,10.3 9.3,11 8.5,11C7.7,11 7,10.3 7,9.5C7,8.7 7.7,8 8.5,8C9.3,8 10,8.7 10,9.5M17,9.5C17,10.3 16.3,11 15.5,11C14.7,11 14,10.3 14,9.5C14,8.7 14.7,8 15.5,8C16.3,8 17,8.7 17,9.5M12,17.23C10.25,17.23 8.71,16.5 7.81,15.42L9.23,14C9.68,14.72 10.75,15.23 12,15.23C13.25,15.23 14.32,14.72 14.77,14L16.19,15.42C15.29,16.5 13.75,17.23 12,17.23Z" /> </svg>';
        hoot();
        setTimeout(function(){switch (num)  {
            case 0:
                t.innerHTML = "Hope you had a good night.";
                hoot();
                break;
            case 1:
                t.innerHTML = "Welcome to my website";
                hoot();
                break;
            default:
            break;
        }}, 3500);
    }
    else if (hour >= 12 && hour < 16) {
        switch (num)  {
            case 0:
                t.innerHTML = "Howdy!";
                hoot();
                break;
            case 1:
                t.innerHTML = "Hello!";
                hoot();
                break;
            default:
            break;
        }
        setTimeout(function(){t.innerHTML = 'Good afternoon  <svg style="width:24px;height:24px" viewBox="0 0 24 24">     <path fill="#fff" d="M20,12A8,8 0 0,0 12,4A8,8 0 0,0 4,12A8,8 0 0,0 12,20A8,8 0 0,0 20,12M22,12A10,10 0 0,1 12,22A10,10 0 0,1 2,12A10,10 0 0,1 12,2A10,10 0 0,1 22,12M10,9.5C10,10.3 9.3,11 8.5,11C7.7,11 7,10.3 7,9.5C7,8.7 7.7,8 8.5,8C9.3,8 10,8.7 10,9.5M17,9.5C17,10.3 16.3,11 15.5,11C14.7,11 14,10.3 14,9.5C14,8.7 14.7,8 15.5,8C16.3,8 17,8.7 17,9.5M12,17.23C10.25,17.23 8.71,16.5 7.81,15.42L9.23,14C9.68,14.72 10.75,15.23 12,15.23C13.25,15.23 14.32,14.72 14.77,14L16.19,15.42C15.29,16.5 13.75,17.23 12,17.23Z" /> </svg>';hoot();}, 3500);
        
    }
    else {
        switch (num)  {
            case 0:
                t.innerHTML = "Hello there!";
                hoot();
                break;
            case 1:
                t.innerHTML = "Welcome!";
                hoot();
                break;
            default:
            break;
        }
        setTimeout(function(){t.innerHTML = 'Good evening  <svg style="width:24px;height:24px" viewBox="0 0 24 24">     <path fill="#fff" d="M20,12A8,8 0 0,0 12,4A8,8 0 0,0 4,12A8,8 0 0,0 12,20A8,8 0 0,0 20,12M22,12A10,10 0 0,1 12,22A10,10 0 0,1 2,12A10,10 0 0,1 12,2A10,10 0 0,1 22,12M10,9.5C10,10.3 9.3,11 8.5,11C7.7,11 7,10.3 7,9.5C7,8.7 7.7,8 8.5,8C9.3,8 10,8.7 10,9.5M17,9.5C17,10.3 16.3,11 15.5,11C14.7,11 14,10.3 14,9.5C14,8.7 14.7,8 15.5,8C16.3,8 17,8.7 17,9.5M12,17.23C10.25,17.23 8.71,16.5 7.81,15.42L9.23,14C9.68,14.72 10.75,15.23 12,15.23C13.25,15.23 14.32,14.72 14.77,14L16.19,15.42C15.29,16.5 13.75,17.23 12,17.23Z" /> </svg>';hoot();}, 3500);
    }
}

function dice(num, element){
    switch(num){
        case 0:element.innerHTML = '<svg  viewBox="0 0 24 24"><path fill="rgba(0,0,0,0.6)" d="M5,3H19A2,2 0 0,1 21,5V19A2,2 0 0,1 19,21H5A2,2 0 0,1 3,19V5A2,2 0 0,1 5,3M12,10A2,2 0 0,0 10,12A2,2 0 0,0 12,14A2,2 0 0,0 14,12A2,2 0 0,0 12,10Z" /></svg>';break;
        case 1:element.innerHTML = '<svg  viewBox="0 0 24 24"><path fill="rgba(0,0,0,0.6)" d="M5,3H19A2,2 0 0,1 21,5V19A2,2 0 0,1 19,21H5A2,2 0 0,1 3,19V5A2,2 0 0,1 5,3M7,5A2,2 0 0,0 5,7A2,2 0 0,0 7,9A2,2 0 0,0 9,7A2,2 0 0,0 7,5M17,15A2,2 0 0,0 15,17A2,2 0 0,0 17,19A2,2 0 0,0 19,17A2,2 0 0,0 17,15Z" /></svg>';break;
        case 2:element.innerHTML = '<svg  viewBox="0 0 24 24"><path fill="rgba(0,0,0,0.6)" d="M5,3H19A2,2 0 0,1 21,5V19A2,2 0 0,1 19,21H5A2,2 0 0,1 3,19V5A2,2 0 0,1 5,3M12,10A2,2 0 0,0 10,12A2,2 0 0,0 12,14A2,2 0 0,0 14,12A2,2 0 0,0 12,10M7,5A2,2 0 0,0 5,7A2,2 0 0,0 7,9A2,2 0 0,0 9,7A2,2 0 0,0 7,5M17,15A2,2 0 0,0 15,17A2,2 0 0,0 17,19A2,2 0 0,0 19,17A2,2 0 0,0 17,15Z" /></svg>';break;
        case 3:element.innerHTML = '<svg  viewBox="0 0 24 24"><path fill="rgba(0,0,0,0.6)" d="M5,3H19A2,2 0 0,1 21,5V19A2,2 0 0,1 19,21H5A2,2 0 0,1 3,19V5A2,2 0 0,1 5,3M7,5A2,2 0 0,0 5,7A2,2 0 0,0 7,9A2,2 0 0,0 9,7A2,2 0 0,0 7,5M17,15A2,2 0 0,0 15,17A2,2 0 0,0 17,19A2,2 0 0,0 19,17A2,2 0 0,0 17,15M17,5A2,2 0 0,0 15,7A2,2 0 0,0 17,9A2,2 0 0,0 19,7A2,2 0 0,0 17,5M7,15A2,2 0 0,0 5,17A2,2 0 0,0 7,19A2,2 0 0,0 9,17A2,2 0 0,0 7,15Z" /></svg>';break;
        case 4:element.innerHTML = '<svg  viewBox="0 0 24 24"><path fill="rgba(0,0,0,0.6)" d="M5,3H19A2,2 0 0,1 21,5V19A2,2 0 0,1 19,21H5A2,2 0 0,1 3,19V5A2,2 0 0,1 5,3M7,5A2,2 0 0,0 5,7A2,2 0 0,0 7,9A2,2 0 0,0 9,7A2,2 0 0,0 7,5M17,15A2,2 0 0,0 15,17A2,2 0 0,0 17,19A2,2 0 0,0 19,17A2,2 0 0,0 17,15M17,5A2,2 0 0,0 15,7A2,2 0 0,0 17,9A2,2 0 0,0 19,7A2,2 0 0,0 17,5M12,10A2,2 0 0,0 10,12A2,2 0 0,0 12,14A2,2 0 0,0 14,12A2,2 0 0,0 12,10M7,15A2,2 0 0,0 5,17A2,2 0 0,0 7,19A2,2 0 0,0 9,17A2,2 0 0,0 7,15Z" /></svg>';break;
        case 5:element.innerHTML = '<svg  viewBox="0 0 24 24"><path fill="rgba(0,0,0,0.6)" d="M5,3H19A2,2 0 0,1 21,5V19A2,2 0 0,1 19,21H5A2,2 0 0,1 3,19V5A2,2 0 0,1 5,3M7,5A2,2 0 0,0 5,7A2,2 0 0,0 7,9A2,2 0 0,0 9,7A2,2 0 0,0 7,5M17,15A2,2 0 0,0 15,17A2,2 0 0,0 17,19A2,2 0 0,0 19,17A2,2 0 0,0 17,15M17,10A2,2 0 0,0 15,12A2,2 0 0,0 17,14A2,2 0 0,0 19,12A2,2 0 0,0 17,10M17,5A2,2 0 0,0 15,7A2,2 0 0,0 17,9A2,2 0 0,0 19,7A2,2 0 0,0 17,5M7,10A2,2 0 0,0 5,12A2,2 0 0,0 7,14A2,2 0 0,0 9,12A2,2 0 0,0 7,10M7,15A2,2 0 0,0 5,17A2,2 0 0,0 7,19A2,2 0 0,0 9,17A2,2 0 0,0 7,15Z" /></svg>';break;
        default:break;
    }
}

function onStart(hour){
    GreetUser(hour);
    var $ = document.getElementById("dice");
    var d = rand(0,6);
    dice(d, $);
}

document.getElementById("dice").onclick = function(){
    var $ = document.getElementById("dice");
    var num = rand(0,5);
    dice(num, $);
    var x = document.getElementsByTagName("svg")[0];
    x.setAttribute("class", "spin");
    setTimeout(function(){x.removeAttribute("class")}, 500);
};

setInterval(DateTime, 1000);
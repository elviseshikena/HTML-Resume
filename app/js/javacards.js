var mq = window.matchMedia("(min-width: 490px)");
var which;
function cardclick(x) {
    var blur = document.getElementsByTagName("blur")[0];
    var screen = document.getElementsByTagName("screen")[0];
    var exit = document.getElementsByTagName("exit-box")[0];
    var header = document.getElementsByTagName("header")[0];
    var cardsTag = document.getElementsByTagName("cards")[0];
    var footer = document.getElementsByTagName("footer")[0];
    var board = document.getElementsByTagName("board")[0];
    if (mq.matches) {
        board.style.filter = "blur(5px)";
        screen.style.display = "-webkit-flex";
        header.style.filter = "blur(5px)";
        cardsTag.style.filter = "blur(5px)";
        footer.style.filter = "blur(5px)";        
    }
    else{
        header.style.filter = "blur(5px)";
        cardsTag.style.filter = "blur(5px)";
        footer.style.filter = "blur(5px)";
        screen.style.display = "block";
    }
    var element = viewbox(x);
    if (x == 1){
        element.style.display = "-webkit-flex";
    }
    else {
        element.style.display = "block";
    }
    element.removeAttribute("class", "viewbox cardhide");
    element.setAttribute("class", "viewbox cardreveal");

    //choose the width and height to assign to exitbox
    //if greater than 490px

    // media query change
    if (mq.matches) {
        switch(x) {
            case 1:exit.style.width = '435px';break;
            case 2:exit.style.width = '535px';break;
            case 3:exit.style.width = '655px';break;
            case 4:exit.style.width = '635px';break;
            case 5:exit.style.width = '655px';break;
            default:exit.style.width = '90vw';break;
        }
    } 
    else {
        // window width is less than 500px   
            exit.style.width = '100vw';
    }
};

function viewbox(index){
    array = [
        document.getElementById("cv-viewbox"),
        document.getElementById("user-viewbox"),
        document.getElementById("projects-viewbox"),
        document.getElementById("experience-viewbox"),
        document.getElementById("certificate-viewbox"),
        document.getElementById("milestones-viewbox")
    ]
    return array[index];
};

var cards = document.getElementsByClassName("card");
//Click a card to blur
document.getElementById("mobile-user-tab").onclick = function(){cardclick(1); which = 1};
cards[1].onclick = function(){cardclick(1); which = 1};
cards[2].onclick = function(){cardclick(2); which = 2};
cards[3].onclick = function(){cardclick(3); which = 3};
cards[4].onclick = function(){cardclick(4); which = 4};
cards[5].onclick = function(){cardclick(5); which = 5};

//click exit to go back and unblur
document.getElementById("exit").onclick = function(){
    var board = document.getElementsByTagName("board")[0];
    var screen = document.getElementsByTagName("screen")[0];

    var header = document.getElementsByTagName("header")[0];
    var cardsTag = document.getElementsByTagName("cards")[0];
    var footer = document.getElementsByTagName("footer")[0];
    var element = viewbox(which);
    element.removeAttribute("class", "viewbox cardreveal");
    element.setAttribute("class", "viewbox cardhide");
    setTimeout(function (){
            element.style.display = "none";
            if (mq.matches) {
                screen.style.display = "none";
                board.style.filter = "none";
                cardsTag.style.filter = "none";
                footer.style.filter = "none";
                header.style.filter = "none"
            }
            else{
                screen.style.display = "none";
                cardsTag.style.filter = "none";
                footer.style.filter = "none";
                header.style.filter = "none"
            }
        }, 300);
    
};
//Scripts for information on cards
var mq = window.matchMedia("(min-width: 490px)");

//My age calculation
function convert(value) {
    //convert seconds to standard format
    var  years, months, days, hours, minutes, seconds, 
    years_, months_, days_, hours_, minutes_, seconds_,
    tTime1, tTime2, tTime3, tTime4, tTime5, tTime6, z;

    tTime1 = value / (60 * 60 * 24 * 365.242199);
    years = parseInt(tTime1);

    years_ = tTime1 - years;

    tTime2 = years_ * 12;
    months = parseInt(tTime2);

    months_ = tTime2 - months;

    tTime3 = months_ * 30.4167;
    days = parseInt(tTime3);
        
    days_ = tTime3 - days;
    
    tTime4 = days_ * 24;
    hours = parseInt(tTime4);
    
    hours_ = tTime4 - hours;
    
    tTime5 = hours_ * 60;
    minutes = parseInt(tTime5);
    
    minutes_ = tTime5 - minutes;
    
    tTime6 = minutes_ * 60;
    seconds = parseInt(tTime6);

    z = [years, months, days, hours, minutes, seconds];

    return z;
}
function printAge() {
    //get birthdate
    birthyear=1997;
    birthmonth=5;
    birthday=9;

    obj = new Date();
    nowY = obj.getFullYear();
    nowM = obj.getMonth() + 1;
    nowD = obj.getDate();
    nowH = obj.getHours();
    nowm = obj.getMinutes();
    nowS = obj.getSeconds();

    //convert birthdate to secs
    year_to_seconds = 60 * 60 * 24 * 365.242199;month_to_seconds = 60 * 60 * 24 * 30.4167;day_to_seconds = 60 * 60 * 24;hour_to_seconds = 60 * 60;minute_to_seconds = 60;
    year = birthyear * year_to_seconds;
    month = birthmonth * month_to_seconds;
    day = birthday * day_to_seconds;
    birthDateSeconds = year + month + day;

    //convert now date-time to seconds
    snYear = nowY * year_to_seconds;
    snMonth = nowM * month_to_seconds;
    snDay = nowD * day_to_seconds;
    snHour = nowH * hour_to_seconds;
    snMinute = nowm * minute_to_seconds;
    nowDateSeconds = snYear + snMonth + snDay + snHour + snMinute + nowS;

    ageSeconds = nowDateSeconds - birthDateSeconds;

    age = convert(ageSeconds);
    ageObj = document.getElementById("age");
    ageObj.innerHTML = age[0];
    ageObj.style.color = '#999';
}
printAge();

//Pages text
document.getElementById("user-cardbox-text").innerHTML = ('I am a student with a passion for all things digital and mechanical. I am currently pursuing an undergraduate degree in <a style="text-decoration:none; font-weight:600; color: maroon; "href="https://www.sfu.ca/mechatronics/about-mse.html"target="_blank">Mechatronic Systems Engineering</a> at Simon Fraser University.<br><br> In my spare time I enjoy designing, creating as well as tinkering with software, playing the piano, drawing and playing table tennis. ');//User

//exit button
document.getElementById("exit").innerHTML = '<svg style="width:24px;height:24px" viewBox="0 0 24 24"><path style="transition: fill 0.5s" fill="rgba(231, 24, 24, 0.5)" d="M12,2C17.53,2 22,6.47 22,12C22,17.53 17.53,22 12,22C6.47,22 2,17.53 2,12C2,6.47 6.47,2 12,2M15.59,7L12,10.59L8.41,7L7,8.41L10.59,12L7,15.59L8.41,17L12,13.41L15.59,17L17,15.59L13.41,12L17,8.41L15.59,7Z" /></svg>'

//Project Cards
var btn = document.getElementsByClassName("resize");
var crdbx = document.getElementsByClassName("cardbox projects-cardbox");

//Card 1
btn[0].onclick = forklift;
btn[1].onclick = forklift;
//Card 2
btn[2].onclick = barcode;
btn[3].onclick = barcode;
//Card 3
btn[4].onclick = sumo;
btn[5].onclick = sumo;
//Card 4
btn[6].onclick = titanic;
btn[7].onclick = titanic;
//Card 5
btn[8].onclick = clock;
btn[9].onclick = clock;
//Card 6
btn[10].onclick = alu;
btn[11].onclick = alu;
//Card 7
btn[12].onclick = track;
btn[13].onclick = track;
//Card 8
//Card 9

//Project Fucntions
function icon(num1, num2, choice){
    var unfold = '<svg style="width:24px;height:24px" viewBox="0 0 24 24"><path fill="#000" d="M12,18.17L8.83,15L7.42,16.41L12,21L16.59,16.41L15.17,15M12,5.83L15.17,9L16.58,7.59L12,3L7.41,7.59L8.83,9L12,5.83Z" /></svg>';
    var fold = '<svg style="width:24px;height:24px" viewBox="0 0 24 24"><path fill="#000" d="M16.59,5.41L15.17,4L12,7.17L8.83,4L7.41,5.41L12,10M7.41,18.59L8.83,20L12,16.83L15.17,20L16.58,18.59L12,14L7.41,18.59Z"></path></svg>';
    switch (choice) {
        case 0: btn[num1].innerHTML = fold; break;
        case 1: btn[num1].innerHTML = unfold; break;
        default: break;
    }
}
function forklift(){
    if (crdbx[0].style.height == '209px') {
        if (mq.matches) {
            crdbx[0].style.height = '931px';
        }else{
            crdbx[0].style.height = '970px';
        }
        icon(0,1,0);
        btn[0].style.background = '#fff';
        btn[1].style.background = '#fff';
    }
    else {
        crdbx[0].style.height = '209px';
        icon(0,1,1);
        btn[0].style.background = '#d1d1d1';
    }
}
function barcode(){
    if (crdbx[1].style.height == '196px') {
        if (mq.matches) {
            crdbx[1].style.height = '679px';
        }
        else{
            crdbx[1].style.height = '775px'; 
        }
        icon(2,3,0);
        btn[2].style.background = '#fff';
        btn[3].style.background = '#fff';
    }
    else {
            crdbx[1].style.height = '196px';
            icon(2,3,1);
            btn[2].style.background = '#d1d1d1';
        }
}
function sumo(){
    if (crdbx[2].style.height == '179px') {
        if(mq.matches){
            crdbx[2].style.height = '929px';
        }
        else {
            crdbx[2].style.height = '969px';
        }
        icon(4,5,0);
        btn[4].style.background = '#fff';
        btn[5].style.background = '#fff';
    }
    else {
        crdbx[2].style.height = '179px';
        icon(4,5,1);
        btn[4].style.background = '#d1d1d1';
    }
}
function titanic(){
    if (crdbx[3].style.height == '165px') {
        if(mq.matches){
            crdbx[3].style.height = '649px';
        }
        else{
            crdbx[3].style.height= '817px';
        }
        icon(6,7,0);
        btn[6].style.background = '#fff';
        btn[7].style.background = '#fff';
    }
    else {
        crdbx[3].style.height = '165px';
        icon(6,7,1);
        btn[6].style.background = '#d1d1d1';
    }
}
function clock(){
    if (crdbx[4].style.height == '226px') {
        if(mq.matches) {
            crdbx[4].style.height = '585px';
        }
        else{
            crdbx[4].style.height = '681px';
        }
        icon(8,9,0);
        btn[8].style.background = '#fff';
        btn[9].style.background = '#fff';
    }
    else {
        crdbx[4].style.height = '226px';
        icon(8,9,1);
        btn[8].style.background = '#d1d1d1';
    }
}
function alu(){
    if(mq.matches){
        if (crdbx[5].style.height == '165px') {
            crdbx[5].style.height = '601px';
            icon(10,11,0);
            btn[10].style.background = '#fff';
            btn[11].style.background = '#fff';
        }
        else {
        crdbx[5].style.height = '165px';
        icon(10,11,1);
        btn[10].style.background = '#d1d1d1';
        }
    }
    else{
        if (crdbx[5].style.height == '194px') {
            crdbx[5].style.height = '726px';
            icon(10,11,0);
            btn[10].style.background = '#fff';
            btn[11].style.background = '#fff';
        }
        else {
            crdbx[5].style.height = '194px';
            icon(10,11,1);
            btn[10].style.background = '#d1d1d1';
        }
    }
}
function track(){
    if (crdbx[6].style.height == '196px') {
        if(mq.matches) {
            crdbx[6].style.height = '667px';
        }
        else {
            crdbx[6].style.height = '763px';
        }
        icon(12,13,0);
        btn[12].style.background = '#fff';
        btn[13].style.background = '#fff';
    }
    else {
        crdbx[6].style.height = '196px';
        icon(12,13,1);
        btn[12].style.background = '#d1d1d1';
    }
}